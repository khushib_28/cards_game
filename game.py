import pygame
import sys
import random

# Initialize Pygame
pygame.init()

# Constants
SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 700
FPS = 60
WHITE = (255, 255, 255)
RED = (255, 0, 0)

# Set up the game screen
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Diamond Bidding Game")
clock = pygame.time.Clock()

class Card:
    def __init__(self, suit, value, position):
        self.suit = suit
        self.value = value
        self.position = position
        self.is_selected = False

    def draw(self, screen):
        card_color = (255, 255, 255)
        if self.is_selected:
            card_color = (200, 200, 200)  # Highlight selected card
        pygame.draw.rect(screen, card_color, self.position)
        pygame.draw.rect(screen, (0, 0, 0), self.position, 2)
        font = pygame.font.Font(None, 32)  # Increase font size
        text = font.render(f"{self.value} {self.suit}", True, (0, 0, 0))
        text_rect = text.get_rect(center=self.position.center)
        screen.blit(text, text_rect)

def create_deck():
    suits = ["Spades", "Hearts"]  # Only include Spades and Hearts
    values = ["Ace", "King", "Queen", "Jack", "10", "9", "8", "7", "6", "5", "4", "3", "2"]
    deck = [Card(suit, value, pygame.Rect(0, 0, 0, 0)) for suit in suits for value in values]
    random.shuffle(deck)
    return deck

class Player:
    def __init__(self, name, assigned_suit, position):
        self.name = name
        self.assigned_suit = assigned_suit
        self.hand = []
        self.bids = []
        self.score = 0
        self.position = position

    def draw_hand(self, screen):
        for i, card in enumerate(self.hand):
            card.position = pygame.Rect(self.position[0] + i * 60, self.position[1], 80, 120)  # Adjust card block size
            card.draw(screen)

def create_deck():
    suits = ["Spades", "Hearts", "Clubs"]
    values = ["Ace", "King", "Queen", "Jack", "10", "9", "8", "7", "6", "5", "4", "3", "2"]
    deck = [Card(suit, value, pygame.Rect(0, 0, 0, 0)) for suit in suits for value in values]
    random.shuffle(deck)
    return deck

def deal_cards(deck, players):
    for player in players:
        player.hand = deck[:13]
        deck = deck[13:]

def draw_bid_card(screen, card):
    pygame.draw.rect(screen, RED, (SCREEN_WIDTH // 2 - 40, SCREEN_HEIGHT // 2 - 60, 80, 120))  # Adjust bid card size
    font = pygame.font.Font(None, 32)  # Increase font size
    text = font.render(f"{card.value} {card.suit}", True, (255, 255, 255))
    text_rect = text.get_rect(center=(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2))
    screen.blit(text, text_rect)

# Adjust other parts of your code as needed to ensure proper sizing and display of card elements.

def handle_bidding(player, clicked_card):
    for card in player.hand:
        if card.position.collidepoint(clicked_card.pos):
            card.is_selected = True
            player.bids.append(card)
        else:
            card.is_selected = False

def game_loop():
    players = [
        Player("Player 1", "Spades", (150, 50)),  # Top player
        Player("Player 2", "Hearts", (150, SCREEN_HEIGHT - 150)),  # Bottom player
    ]
    deck = create_deck()
    deal_cards(deck, players)

    bidding_round = 1
    current_bid_card = deck.pop(0)  # Get the first card for bidding

    running = True

    while running:
        # Event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                for player in players:
                    if player.position[1] < event.pos[1] < player.position[1] + 90:
                        handle_bidding(player, event)

        # Clear the screen
        screen.fill(WHITE)

        # Draw player hands
        for player in players:
            player.draw_hand(screen)

        # Draw current bidding card
        draw_bid_card(screen, current_bid_card)

        # Update the display
        pygame.display.flip()

        # Cap the frame rate
        clock.tick(FPS)

    pygame.quit()
    sys.exit()

if __name__ == "__main__":
    game_loop()
